#include <iostream>

using namespace std;

/** Helper Class that returns the type
    of what the Iterator is pointing to
    For pointer Iterators of type T*
    it returns T (see specialization below)
    for Other iterator type it returns
    the typedef value_type
    */
template<class T>
struct ReturnType
{
    typedef typename T::value_type type;
};
/** Partial Specialization for Pointer types
*/
template<class T>
struct ReturnType<T*>
{
    typedef T type;
};


/** accumulate function. gets iterator to first and last element as well as a Functor
   that describes how the sequence elements are accumulated
   */
template<class T, class FuncT>
typename ReturnType<T>::type
   accum(T begin, T end, FuncT functor)
{
    typedef typename ReturnType<T>::type ReturnT;
    if(begin == end)
      return 0;
    ReturnT result = *begin;
    T iter = begin;
    ++iter;
    for(; iter!= end; ++iter)
    {
        result = functor(result,*iter);
    }
    return result;
}

/** Sum functor to be used with accumulate
*/
template<class T>
struct Sum
{
    T operator()(T lhs, T rhs)
    {
        return lhs+rhs;
    }
};

/** Product Functor
*/
template<class T>
struct Multiply
{
    T operator()(T lhs, T rhs)
    {
        return lhs*rhs;
    }
};


/** Simple Array class using dynamic memory allocation
*/
template<class T>
struct Array
{
    typedef T* iterator;
    T* arr;
    int size;

    Array():arr(0), size(0)
    {}

    void push_back(T in)
    {
        //create new memory
        T* newmem = new T[size+1];
        // copy old values
        for(int ii = 0; ii < size; ++ii)
            newmem[ii] = arr[ii];
        // copy new value
        newmem[size] = in;
        // incr size
        size+=1;
        // free space
        delete [] arr;
        // set arr pointer
        arr = newmem;
    }

    T* begin()
    {
        return arr;
    }

    T*  end()
    {
        return arr+size;
    }

};

/** Forward List Element
*/
template<class T>
struct ListElement
{
    T value;
    ListElement<T>* next;
    ListElement(T val):value(val), next(0)
    {}

};


/** Iterator Object (generalized Pointer)
   with ++, ==, != and * (dereferencing) operator
   overloaded
*/
template<class T>
struct ListIterator
{
    typedef T value_type;
    ListElement<T> * current;
    ListIterator(ListElement<T>* element)
    : current(element)
    {}

    bool operator ==(ListIterator & rhs)
    {
        return rhs.current == current;
    }
    bool operator !=(ListIterator & rhs)
    {
        return !(rhs.current == current);
    }

    ListIterator & operator++()
    {
        current = current->next;
        return *this;
    }

    T & operator*()
    {
        return current->value;
    }
};

/** actual list
*/
template<class T>
struct List
{
    typedef ListIterator<T> Iterator;

    ListElement<T>* first;
    ListElement<T>* last;
    int size;
    List():first(0), last(0), size(0)
    {}

    /** Add element to list*/
    void push_back(T in)
    {
        // create new List Element
        ListElement<T> * newElement
            = new ListElement<T>(in);
        // Special case: Size = 0;
        if(size == 0)
        {
            first = newElement;
            last = newElement;
        }
        else
        {
            // attach new Element to last element
            last->next = newElement;
            // set new Element to be last element
            last = newElement;
        }
        size+=1;
    }

    Iterator begin()
    {
        return Iterator(first);
    }

    Iterator end()
    {
        return Iterator(0);
    }
};

int main()
{
    //Create sequence
    //typedef Array<int> MySequence;
    typedef int       MyType;
    typedef List<MyType> MySequence;
    MySequence sequence;

    // Add elements
    sequence.push_back(1);
    sequence.push_back(2);
    sequence.push_back(4);

    // output accumulated Sum
    cout <<
       ::accum(sequence.begin(),
                  sequence.end(),
                  Sum<MyType>()) << endl;
    // output accumulated Product
    cout <<
       ::accum(sequence.begin(),
                  sequence.end(),
                  Multiply<MyType>()) << endl;
}

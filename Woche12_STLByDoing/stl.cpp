#include <iostream>

using namespace std;



template<class T>
struct Array
{
    typedef T* Iterator;
    int size;
    T* data;


    Array():size(0),data(0)
    {}
    ~Array()
    {
        delete [] data;
    }
    /** element am ende des arrays anf�gen
    */
    void push_back(T const & in)
    {
        //neuer speicher
        T* newmem = new T[size+1];
        //alte wertekopieren
        for(int ii = 0; ii < size; ++ii)
        {
            newmem[ii] = data[ii];
        }
        //neuen Wert anf�gen
        newmem[size] = in;
        size+=1;
        //alten speicher freigeben
        delete [] data;
        //pointer richtig setzen
        data = newmem;
    }
        /** iterator auf erstes element*/
    Iterator begin()
    {
        return data;
    }
        /** iterator auf "jenseits des letzten elements"*/
    Iterator end()
    {
         return data+size;
    }
};

/** Design Pattern f�r eine Verkettete liste
*/
template<class T>
struct ListElement
{
    T value;
    ListElement<T> * next;
    ListElement(T const & val)
        :value(val), next(0)
    {}
};


/** Generalisierter Pointer f�r Listen
*/
template<class T>
struct ListIterator
{
    ListElement<T> * current;

    ListIterator(ListElement<T> * ptr)
        :current(ptr)
    {
    }

    /** vergleichs ops*/
    bool operator !=(ListIterator const & rhs)
    {
        return !(current == rhs.current);
    }

    bool operator ==(ListIterator const & rhs)
    {
        return (current == rhs.current);
    }

    /** ++var operator
    * Hinweis f�r var++ waere die signatur
    * ListIterator & operator++(int)
    */
    ListIterator & operator++()
    {
        current = current->next;
        return *this;
    }
    /** dereferzierungs operator *var
    * nicht mit T operator*(T const & rhs) verwechseln (Multiplikation
    */
    T &  operator*()
    {
        return current->value;
    }
};

/** List Klasse - hat das selbe interface wie Array,
verwendet aber eine verkette Liste von ListElement.
*/
template<class T>
struct List
{
    typedef ListIterator<T> Iterator;
    ListElement<T> * first;
    ListElement<T> * last;
    int size;

    List():first(0),last(0), size(0)
    {}

    /** element am ende hinzuf�gen*/
    void push_back(T const & in)
    {
        if(size == 0)//falls noch kein elemnt da ist sind first und last nach dem push back identisch
        {
            first = new ListElement<T>(in);
            last = first;
            ++size;
        }
        else
        {
            //neues elemnt erzuegen
            ListElement<T> * newLast =
                new ListElement<T>(in);
            //pointer richtig verdrahten
            last->next = newLast;
            last = newLast;
            ++size;
        }

    }
    /** Iterator zum ersten element*/
    Iterator begin()
    {
        return Iterator(first);
    }
    /** iterator auf "jenseits des letzten elements"*/
    Iterator end()
    {
        return Iterator(0);
    }
};

/**summe*/
template<class T>
struct Sum
{
    T operator()(T const & rhs, T const & lhs)
    {
        return rhs + lhs;
    }
};

/** product*/
template<class T>
struct Prod
{
    T operator()(T const & rhs, T const & lhs)
    {
        return rhs * lhs;
    }
};


/** sequenz gegeben mit start und end Iterator
(generalisierter Pointer) mit Functor op (zB Sum oder Prod)
accumulieren
*/
template<class Iterator, class Functor>
double accumulate(Iterator begin, Iterator end,
                  Functor op)
{
    double result = *begin;
    Iterator iter = begin;
    ++iter;
    for(;iter!= end; ++iter)
    {
        result = op(result, *iter);
    }
    return result;
}

int main()
{
    //typedef Array<double> MySequenceType;
    typedef List<double> MySequenceType;

    MySequenceType sequence;
    //add elements
    sequence.push_back(1.0);
    sequence.push_back(2.0);
    sequence.push_back(4.0);

    //sum up everything
    cout << accumulate(sequence.begin(),
                       sequence.end(),
                       Sum<double>())
         << endl;
    // multiply
    cout << accumulate(sequence.begin(),
                       sequence.end(),
                       Prod<double>())
         << endl;




    return 0;
}

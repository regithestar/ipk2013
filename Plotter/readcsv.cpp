#include <iostream>
#include <fstream>
#include <string>
#include "conversion.h"
#include "plotdisplay.h"


int main()
{
  std::string line;
  std::ifstream fin("ergebnis.txt");
  int nlines = getNumberOfLines("ergebnis.txt");
  int firstCol[nlines];
  int secondCol[nlines];
  std::cout<< nlines; 
  int x, y;
  int ii = 0; //Zaehlvar
  while(!fin.eof())
  {
    std::getline(fin, line);
    convertCSVline(line, x, y);
    std::cout <<  x<< " " << y <<std::endl;
    firstCol[ii] = x;
    secondCol[ii] = y;
    ++ii;
  }
  plotdisplay::init(10);
  for(int ii = 0; ii < nlines; ++ii)
  {
      plotdisplay::update(firstCol[ii], secondCol[ii], '*');
  }
  plotdisplay::display();

}

#include <iostream>
#include <stdlib.h>
#include <time.h>



/* Namespace with functions for displaying and updating the
   Regentrog
   Usage: 
    plotdisplay::init(10)   // create a display with size 10;
    float x = computeX(....)// compute new raindrop coordinate
        float y = ...;
    plotdisplay::update(x,y);
    ...
    plotdisplay::display();
*/
namespace plotdisplay
{
    /* Memory holding the Ascii Image*/
    char  trog[100][100];
    /* size of the ascii image */
    int size = 0;
    
    /* initialize empty ascii image, size is resolution*/
    void init(int resolution = 25);

    /* update trog image with new point x, y and pixel style c
    */
    void update(int x, int y, char c = '*');

    /* display trog
    */
    void display();

}
/////// AB HIER DEFINITIONEN


void plotdisplay::init(int resolution)// Default argumente werden nur einmal vergeben
{
    size = resolution;
    for(int ii =0; ii < size; ++ii)
    {
        for(int jj = 0; jj < size; ++jj)
        {
            trog[ii][jj] = ' ';
        }
    }
}

/* update trog image with new point x, y and pixel c
*/
void plotdisplay::update(int y, int x, char c)
{
    trog[size-x][y-1] = c;
}

/* display trog
*/
void plotdisplay::display()
{
    for(int ii =0; ii < size; ++ii)
    {
        for(int jj = 0; jj < size; ++jj)
        {
            std::cout << trog[ii][jj];
        }
        std::cout << std::endl;
    }
    std::cout << "Press Enter Key to continue\n";
    std::cin.ignore(0, '\n');
    std::cin.get();
}

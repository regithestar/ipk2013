#include <stdlib.h>
#include <fstream>
#include <string>


/** Take a string in the format xx,yy where xx and yy are integer numbers
    and convert to two integers
    \param line   input string
    \param x,y    output integers
*/
void convertCSVline(std::string line, int &x, int&y);

/** get number of lines in a text file
*/
int getNumberOfLines(std::string filename);





//Ab hier Definition
void convertCSVline(std::string line, int &x, int&y)
{
    std::string a;
    std::string b;
    int commaposition = 0;
    for(int ii =0; ii< line.size(); ++ii)
    {
        if(line[ii] == ',')
           commaposition = ii;
    }
    for(int ii =0 ; ii < line.size(); ++ii)
    {
        if(ii < commaposition)
           a = a+ line[ii];
    else if(ii > commaposition)
       b = b+ line[ii];
    }
    x = atoi(a.c_str());
    y = atoi(b.c_str());
}


int getNumberOfLines(std::string filename)
{
    std::string line;
    std::ifstream fin(filename.c_str());
    int result = 0;
    while(!fin.eof())
    {
        ++result;
        std::getline(fin, line);
    }
    return result;
}

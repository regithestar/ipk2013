#ifndef WESEN_HXX
#define WESEN_HXX
#include <iostream>
class Wesen
{
public:
    template<class T>
    void dosomething();
};

template<class T>
void Wesen::dosomething()
    {
    std::cout << "Wesen::dosomething() called.\n";
    }


#endif//WESEN_HXX

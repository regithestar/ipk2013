filestruktur - siehe pdf 

Wesen.h wird aus 2 verscheidenen cpp dateien aus eingefügt: Spiellogik und Displaylogik.
Kompilier kommandos sind in make.sh
compiliert: 			 Die implementierung von Wesen ist in einer 3.  cpp datei Wesen.cpp 	
compiliert nicht:		 Die implementierung ist in der Header datei - linker fehler
compiliert_explicit_inline: 	 wie compiliert nicht mit dem zusatz inline vor der function in Wesen.h 
				 (function wird nicht verlinkt sondern im code reinkopiert)
compiliert_implicit_inline:	 Alle Funktionen im Class block sind implizit inline
compiliert_templates:		 Bei templates MUSS die implementierung in der headerdatei sein 
				(beachtet auch die unterschiede in displaylogik.cpp und spiellogik.cpp ->
			 	 wesen.dosomething<int>() statt wesen.dosomething())
compiliert_nicht_templates:	 Was passiert wenn die implementierung bei templates in die cpp datei geschrieben wird.
#include <iostream>


int WesenID = 0;


char* constructWesen()
{
    int* data = new int[2];
    data[1] = WesenID;
    WesenID+=1;
    return (char*)data;
};

void setWesenAge(char* wesen, int age)
{
    int* reinterpretWesen = (int*)wesen;
    reinterpretWesen[0] = age;
}

int getWesenAge(char* wesen)
{
    int* reinterpretWesen = (int*)wesen;
    return reinterpretWesen[0];
}

int getWesenID(char* wesen)
{
    int* reinterpretWesen = (int*)wesen;
    return reinterpretWesen[1];
}

void destructWesen(char* wesen)
{
    int* reinterpretWesen = (int*)wesen;
    delete [] reinterpretWesen;
}

int main()
{
    char* wesen1 = constructWesen();
    setWesenAge(wesen1, 22);
    std::cout << "age: " << getWesenAge(wesen1) << " id: " << getWesenID(wesen1);
    destructWesen(wesen1);
    return 0;
}

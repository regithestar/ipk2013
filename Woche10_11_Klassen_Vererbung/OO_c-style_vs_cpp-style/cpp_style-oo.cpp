#include <iostream>
struct Wesen
{
    static int nextID;

    int age;
    int id;

    Wesen():age(0), id(Wesen::nextID)
    {
        Wesen::nextID +=1;
    }

    int getAge()
    {
        return this->age;
    }

    void setAge(int age)
    {
        this->age = age;
    }

    int getID()
    {
        return this->id;
    }
};

int Wesen::nextID = 0;

int main()
{
    Wesen wesen1;
    wesen1.setAge(22);
    std::cout << "age: " << wesen1.getAge() << " id: " << wesen1.getID();
    //destructor called implicitely
    return 0;
}

#include <iostream>

/** Code kompiliert.
 *  Benutzt std::cout um die werte der variablen nach verschiedenen
 *  anweisungen anzuschauen
 */

/** Hilfsfunktion um 2^n zu bestimmen;
 */
int pow2(int n)
{
    int result = 1;
    for(int ii = 0; ii < n; ++ii)
        result *=2;
    return result;
}

int main()
{
    // integer types haben vorzeichen
    // https://de.wikipedia.org/wiki/Integer_%28Datentyp%29
    int   var_int   =  1;
    signed int var_int2 = 2;  // signed wird implizit angenommen. signed int = int;
    short var_short =  -102;
    char  var_char  =  -32;
    char  var_char2 =  'a';  // functioniert wegen ascii tabelle -siehe https://de.wikipedia.org/wiki/ASCII
    long  var_long  = 12321;

    // typen unterscheiden sich in anzahl der bytes
    std::cout << "size of int :" << sizeof(int) << "bytes\n";
    std::cout << "size of int variable :" << sizeof(var_int) << "bytes\n"; // Ebenso gültig
    std::cout << "size of short :" << sizeof(short) << "bytes\n";
    std::cout << "size of char :" << sizeof(char) << "bytes\n";
    std::cout << "size of long :" << sizeof(long) << "bytes\n";
    std::cout << "size of int :" << sizeof(int) << "bytes\n";

    // 1 byte = 8 bits integer werden binaer codiert https://de.wikipedia.org/wiki/Dualsystem
    // mit n bits können 2^n   zahlen dargestellt werden.
    // bei signed integer typen wird ein bit für das vorzeichen verwendet.
    // bsp short (2bytes, 16 bits)
    short short_min = -pow2(sizeof(short)-1);
    short short_max = pow2(sizeof(short)-1)-1;
    std::cout << "short min " << short_min
              << " short max " << short_max << std::endl;
    short_max+=1;                                           //overflow
    short_min-=1;                                           //overflow
    std::cout << "short min overflow " << short_min
              << " short max overflow " << short_max << std::endl;

    /** unsigned integers
     */
    unsigned int   var_uint   = 1;
    unsigned short var_ushort = 2;
    unsigned char  var_uchar  = 3;
    unsigned long  var_ulong  = 4;

    // haben die selbe groesse wie ihre signed counterparts.
    // (sizeof(int) == sizeof(unsigned int)) ist true
    //  Der range in positive richtung verdoppelt sich durch fehlende vorzeichenbit
    unsigned short ushort_max = pow2(sizeof(unsigned short))-1;
    std::cout << "unsigned short max:" << ushort_max << std::endl;
    ushort_max +=1;                                         // overflow
    std::cout << "unsigned short max overflow:" << ushort_max << std::endl;
    ushort_max = -1;                                         // underflow
    std::cout << "unsigned short max underflow:" << ushort_max << std::endl;

    /** boolsche variablen
     meistens sizeof(bool) == sizeof(char);
    */
    bool var_bool_true = true;
    bool var_bool_false = false;
    var_bool_true = 1;
    var_bool_false = 0;

    /** Gleitkomma zahlen float und double.
     * double hat doppelt soviele bits zur darstellung als float
     *https://de.wikipedia.org/wiki/Gleitkommazahl
     */
    float  var_float = 2.2f;
    double var_double = 2.31222;
    std::cout << "Size of float " << sizeof(float) << " bytes\n";
    std::cout << "Size of double " << sizeof(double) << " bytes\n";

    /** Implizites casting
     int zu float/double
    */
    unsigned int var_int3 = ~0;          //short form für pow2(sizeof(int)) -1
    double var_double2 = var_int3;       // okay double eine übergruppe von int;
                                         // jedes int kann hinreichend genau von double dargestellt werden
    float  var_float2  = var_int3;       // nicht okay... verlieren infos
    std::cout << "original:" << var_int3
              << " double: " << var_double2
              << " float :" << var_float2 << std::endl;

    // float zu int -> wird immer hinter dem komma abgehakt kein runden!
    var_float = 3.6;
    var_int = var_float;
    std::cout <<" ohne runden : " << var_int <<std::endl;
    // tipp zum runden: +0.5
    var_int = var_float+0.5;
    std::cout <<" mit runden : " << var_int << std::endl;

    // signed --> unsigned
    // Achtung bei negativen zahlen
    var_int = -1;
    var_uint = var_int;
    std::cout << "casted int to unsigned int " << var_uint << std::endl;
    // unsigned --> signed
    // Achtung bei überlauf
    var_ushort = pow2(16)-1;
    var_short = var_ushort;
    std::cout << "casted unsigned short to short " << var_short << std::endl;

    // alles zu bool
    // bool x = a;  x ist true falls a != 0
    bool var_bool2 = -1.0f;
    bool var_bool3 = 0.0f;
    std::cout <<"casting to bool " << var_bool2 << " " <<var_bool3 <<std::endl;

    /** explizites casting wichtig (u.a bei divisitionen)
     */
    var_float = (float)var_int;     // c style;
    var_float = float(var_int);     // c++ constructor syntax (manchmal langsamer).
    var_float = 1.0/var_int;        // integer division
    var_float = 1.0/(float)var_int; // floating division
    return 0;
}

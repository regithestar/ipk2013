#include <iostream>
// Beispiel code für verschiedenste bedingte anweisungen
int main()
{
    int b = 100;                    // Variable
    bool bedingung = b < 100;       // Bedingungen sind boolsche variablen - sie sind wahr oder falsch.


    /** Ternärerer ? : operator
     Nur fuer simple Sachen verwenden
    */
    {//Scope wegen mehreren Beispielen.
        int a = (bedingung)? 100: 200;  // Falls bedingung wahr weise 100 a zu, ansonten 200
        std::cout << a << std::endl;
        a = (b < 100)? 100:200;         // Same as above;
        a = (b < 100)?(b < 50?1:2):3;   // Falls b < 50 soll a 1 sein falls b zwischen 50 und 100 dann soll a
                                        // 2 sein ansonsten soll es 3 sein.
                                        // Sobald ihr das gefühl habt das es den Augen weh tut solltet ihr
                                        // if-else verwenden
    }


    /** if - Code in dem block wird ausgeführt falls bedingung wahr ist
     */
    {
        int a = 2;
        if(a == 2)                      //bedingung
        {
            a = 3;                      // Mehrere anweisungen.
            a += 1;
        }

        if(a == 2)                      // Bei einzelanweisungen kann man {} weglassen;
            a = 3;
    }
    /** if -else ist ?: vorzuziehen sobald die bedingung komplexer ist
     */
    {
        int a = 2;
        if(a == 2)                      //falls a == 2 soll ist a am 4 ansonsten -1;
        {
            a = 3;
            a += 1;
        }
        else
        {
            a = -1;
        }

        if(a == 2)                      //kompakter bei einzelanweisungen
            a = 3;
        else
            a = -1;
    }
    /** if -else if .... ist im gegensatz zu anderen sprachen kein eigenständiges konstrukt:
     */
    {
        int a = 0;
        if(a == 1)                      //falls a ==1 setze a =3, falls a==2 setze a =4, sonst setze a =5;
        {
            a = 3;
        }
        else
        {
            if(a == 2)
            {
                a = 4;
            }
            else
            {
                a = 5;
            }
        }

        if(a == 1)                      // So wie oben aber mit weglassen von {}. der komplette if block gilt
        {                               // als eine einzige anweisung
            a = 3;
        }
        else if(a == 2)
        {
            a = 4;
        }
        else
        {
            a = 5;
        }

        if(a == 1)                      // Das geht nur mit einzelanweisungen sonst {} verwenden
            a = 3;
        else if(a == 2)
            a = 4;
        else
            a = 5;
    }

    /** switch case - bei mehreren if- else if -else if - else mit gleicher variable in bedingung
      ganz nützlich:*/
    {
        int a = 1;
        if( a == 1)
            a = 3;
        else if(a == 2)
            a = 4;
        else if(a == 5)
            a = 5;
        else
            a = 6;

        //Equivalent:
        switch(a)                   // n
        {
            case 1:                 // achtung : und nicht ;
              a = 3;
              break;                // break ist wichtig. ohne break  wird sowohl alles was darauf folgt
            case 2:                 // auch ausgeführt. mehr dazu kommt bald
              a = 4;
              break;
            case 5:
              a = 5;
              break;
            default:
              a = 6;
        }
    }
    /** Beliebte fehler
     */
    {
        int a = 3;
        if(a = 1)                   //immer wahr weil = und nicht ==
        {                           // weil bool b = (a = 1)
            a = 2;                  //           b = a  (a ist 1)
        }                           //           b ist true;

        if(a == 1);                 //; beendet die anweisung das danach wird immer ausgeführt
        {
            a = 2;
        }
    }
    return 0;
}

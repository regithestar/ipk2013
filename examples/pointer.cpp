#include <iostream>
#include <stdlib.h>
//Pointer, Addresse, variablen

using namespace std;

// ignorieren wird weiter unten erkl�rt
void edit_ptr(int* ptr, int size);
int* allocate_mem( int & size);

int main()
{
    // Address of operator &
    int a = 1;
    cout << "Wert von a: "  << a  << endl;
    cout << "Addresse von a:" << &a << endl; //lies address of a

    // References vs address of
    int& b = a;                 // & geh�rt zur typbezeichnung (steht zwischen typ
                                // (in dem fall int) und variablennamen)
    b = 10;
    cout << "Wert von a: " << a << endl;
    cout << "Wert von b(reference): " << b << endl;
    cout << "Addresse von a: " << &a << endl;
    cout << "Addresse von b(reference):" << &b << endl;

    // Weitere Verwechslungsgefahr: logisches und, bitwise und:
    // Das sind beides bin�re operatoren (links und rechts stehen variablen namen oder zahlen)
    int  c = 10;
    int  d = a&c;
    bool e = a&&c;

    // Pointer typen
    int* ptr_a = &a; //lies: kreiere eine variable mit dem namen ptr_a 
                    // vom typ pointer to int und speicher darin die addresse von a;
                    // ptr_a ist an der addresse &ptr_a abgespeichert
    cout << "Wert von ptr_a: "     <<  ptr_a << endl;
    cout << "Wert von a: "         <<      a << endl;
    cout << "Addresse von a: "     <<     &a << endl;
    cout << "Addresse von ptr_a: " << &ptr_a << endl;
    //Dereferenzierung:
    *ptr_a = 20;

    // Vergleich Pointer Referenz
    int & ref_a  = a;            // referenz, einmal erzeugt kann nicht ver�ndert werden
                                 // "const pointer" 
    int * ptr_a2 = &a;           // pointer kann ver�ndert werden
    int * const 
      const_ptr_var_data_a = &a; // das entspricht der referenz. 
                                 // Ein Kompiler kann aber referenzen und const pointer anders
                                 // implementieren.
    const int & const_ref_a = a;  // Read only referenz auf a
    const int * const 
      const_ptr_const_data_a= &a; // Entrsprechendes mit pointer

    // Vergleich Felder, Pointer
    int array1[3] = {1,2,3}; 
    cout << "Addresse a[0]: "  << &array1[0] << endl;
    cout << "Addresse a[1]: "  << &array1[0] << endl;
    cout << "Addresse a[2]: "  << &array1[0] << endl;
    cout << "Wert a: "         << array1     << endl;
    //a == &a[0];
    //Ein Feld ist ein const pointer 
    // a = &b wird einen Fehler auspucken. 
    // pointer kann man genauso wie felder benutzen.
    int* ptr_3 = array1;
    cout << "Wert ptr_3[0]: "  << ptr_3[0] << endl;
    cout << "Wert ptr_3[1]: "  << ptr_3[1] << endl;
    cout << "Wert ptr_3[2]: "  << ptr_3[2] << endl;
    // wichtig der speicher muss vorher reserviert worden sein. 
    // Ansonsten Segmentation fault. 
    // int* aa;
    // *aa = 1323;  // auskommentieren = segmentation fault. aa zeigt auf nicht reservierten Speicher 

    // new/delete
    //new int;  // new int alloziert speicher f�r int ohne einen variablennamen zuvergeben... 
                // ergibt memory leak
    int* dyn_a = new int; //
    // dyn_a = new int   // auskommentieren f�r memory leak .. der speicher der vorher alloziert wurde 
                         // ist verloren
    delete dyn_a;         // gebe speicher wieder frei
    // Fieser memory leak f�hrt wahrscheinlich dazu dass das system ziemlich langsam wird
    // es werden speicher f�r 3000000000 ints erzeugt (30000000000*sizeof(int)) = ca. 12GB speicher
    // for(int ii = 0; ii < 3000000000; ++ii) dyn_a = new int;

    // new delete mit arrays
    int* dyn_array = new int[10];
    delete [] dyn_array;            // der compiler weiss automatisch wieviele zu l�schen sind.

    // wozu ist das gut?
    // Man kann viel mehr dynamischen speicher allozieren als statisch
    //int big_array[78643200]; // entspricht knapp 300 MB by 4 byte int das funktioniert nicht
    //int* big_array2 = new int[78643200]; // Das tut 

    // alternative zu call by reference
    int* ptr2 = new int[10];
    int arr3[10];
    edit_ptr(ptr2, 10);
    edit_ptr(arr3, 10);
    delete [] ptr2; 
    //delete [] arr3; // Error

    // r�ckgabe von feldern aus einer funktion deren gr��e vorher nicht bekannt ist
    // speicher lebt unabh�ngig vom scope... 
    int size;
    int* ptr3 = allocate_mem(size);   //Wird kein speicher kopiert sondern nur der pointer.
    delete [] ptr3;
    return 0;
}

/** alternative zu pass by reference. 
    by feldern die einzige M�glichkeit
*/
void edit_ptr(int* ptr, int size)
{
    for(int ii =0; ii < size; ++ii)
        ptr[ii] = ii;
}

/** Dieses Beispiel ist etwas konstruiert..
Man stelle sich aber eine Tabelle vor, bei dem man nicht weiss, wieviele Eintr�ge existieren
*/
int* allocate_mem( int & size)
{
    size = rand()%1000;
    return new int[size];
}
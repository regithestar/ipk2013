#include <iostream>

// Beachtet die extra scopes, in denen sich die einzelen schleifen beispiele befinden
int main()
{
    // Do while - wird selten verwendet. fuehrt Schleifen code mindestens 1 mal aus
    {
        int ii = 0;                             //Schleifenvariable, Laufvariable
        do
        {
            std::cout << ii << std::endl;
            ++ii;                               //Schleifenincrement
        }
        while(ii < 100);                        //Schleifenbedingung
    }
    // while schleife. Wird verwendet, wenn nicht klar wie oft die schleife 
    // ausgefuehrt wird. (zB Event loop : https://de.wikipedia.org/wiki/Ereignisschleife)
    {
        int ii = 0;                             //Schleifenvariable, Laufvariable
    while(ii < 100)				//Schleifenbedingung
    {
            std::cout << ii << std::endl;
            ++ii;                               //Schleifenincrement
    }
    }
    // goto. Wird (In Schleifen und generell) nicht verwendet.
    // Code wird durch gotos unverstaendlicher.
    // Ausserdem ist die schleife nicht mehr in einer scope
    {
       int ii = 0;
      Anfang:
        std::cout << ii <<std::endl;
        ++ii;                                   //Schleifenincrement
        if(ii < 100) goto Anfang;		//Schleifenbedingung
    }
    // for schleife falls m�glich verwenden da oft vom compiler gut zu optimieren
    // Nur Verwendbar falls die anzahl der iterationen bekannt ist (Oefter als man denkt)
    // �hnlich zu:  int ii = 0;while(bedingung){....;++ii}
    {
    for(int ii =0; ii < 100; ++ii)              // Deklaration der Schleifenvariable; Schleifenbedingung; Increment
    {
         std::cout << ii << std::endl;
    }
    }
    // Geschweifte Klammer oder nicht
    {
        for(int ii = 0; ii < 100; ++ii) 
        std::cout << ii << std::endl;           //funktioniert nur wenn es eine anweisung gibt
    for(int ii = 0; ii < 100; ++ii)
    {
        std::cout << ii << std::endl;           //Mehrere anweisungen.
        std::cout << ii+1 << std::endl;
    }
    }
    // Nested for loops (geht auch mit den anderen schleifen)
    // Die regeln f�r geschweifte klammern gelten immernoch
    {
        for(int ii = 0; ii < 100; ++ii)
        for(int jj = 0; jj < 200 ;++jj)
             std::cout << ii << ", " << jj << std::endl;

        for(int ii = 0; ii < 100; ++ii)
        for(int jj = 0; jj < 200 ;++jj)
        {
             std::cout << ii << ", " << jj << std::endl;
        }
        
    for(int ii = 0; ii < 100; ++ii)
    {
        for(int jj = 0; jj < 200 ;++jj)
        {
             std::cout << ii << ", " << jj << std::endl;
        }
    }
    }
    // Weitere for varianten. In reinem C sind nur solche varianten erlaubt
    {
    {
        int ii;
            for(ii = 0; ii < 100; ++ii)
                std::cout << ii << std::endl;
    }
    {
        int ii = 0;
        for(; ii < 100; ++ii)
            std::cout << ii << std::endl;
    }
    }
    // Mehrere Bedingungen//Laufvariablen
    {
        for(int ii = 0, jj = 5; ii < 100 && jj>=0; ++ii, --jj)	//Achtung Komma(,) und Semicolon (;)
            std::cout << ii << jj << std::endl;			//Achtung2: Nicht mit Nested For verwechseln.
        for(int ii = 0, jj = 5; ii < 100; ++ii)			
            std::cout << ii << std::endl;
    }
    //Endlosschleifen
    {
    //int ii;
        //while(true)
    //{
    // std::cout << ii << std::endl;
    // ++ii;
    //}
    //
    //for(;;)
    //{
    //std::cout << "Hello\n";
    //}
    }
    return 0;
}

#include <iostream>

int main()
{
    //operatoren
    int a = 0;
    int b = 2;
    int g = 0;
    float c = 1.0f;
    float d = 2.0f;
    float e = 0;
    // binäre operationen  = ist der zuweisungs operator
    g = a + b;
    g = a - b;
    g = a / b;                  // ganzzahl division mit rest;
    g = a % b;                  // rest von der ganzzahl division (modulo)
    g = a * b;
    e = c / d;                  // "normale division"
    a += 4;                     // equivalent zu a= a+ 4;
    a -= 4;                     // a = a-4;
    d /= 2.0;                   // d = d/2.0
    d *= 2.0;                   // d = d*2.0
    // binär logische ops;
    bool val1 = true;
    bool val2 = false;
    bool val3 = val1 || val2;   // logisches oder; val3 ist true falls entweder val1
                                // oder val2 wahr ist.
    val3 = val1 && val2;        // logisches und; val3 ist true nur wenn sowohl val1
                                // als auch val2 wahr sind
    val3 = !val1;                // logisches not; val3 ist das Gegenteil von val1;

    // vergleichs operatoren
    val3 = 3 == 4;               // gleich
    val3 = 3 != 4;               // ungleich;
    val3 = 3 <  4;
    val3 = 3 >  4;
    val3 = 3 <= 4;
    val3 = 3 >= 4;

    // bitwise ops
    unsigned char t1 = 0;                // binärdarstellung 00000000  -> dezimal 0;
    unsigned char t2 = ~t1;              // bitwise not ->00000000 -> 11111111;
    unsigned char t3 = 4;                // binär 00000100 dezimal 4
    t2 = 4|2|1;                          // binär 00000100|00000010|00000001 = 00000111; dezimal 7
    t3 = ~1;                              // binär ~(00000001) = 11111110;
    t1 = t3&t2;                          // 11111110 & 00000111 = 00000110   ; dezimal 6;


    // unaere ops

    int inc = 0;
    int res = 0;
    res = ++inc;                         // res = 1; inc = 1
    res = inc++;                         // res = 1; inc = 2;
    --inc;
    inc--;
    return 0;
}

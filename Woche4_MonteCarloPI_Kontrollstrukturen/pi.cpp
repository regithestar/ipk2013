#include <iostream>                     // Input Output STREAMs
#include <stdlib.h>                     // standard library enthaelt rand() und srand()
#include <time.h>                       // enthaelt code im time() aufzurufne
//#include "trogdisplay.h"                // code um den Trog darzustellen
//some more comments
///more stuff
using namespace std;

/** Spuckt einen float zwischen 0.0 und 1.0 aus
*/
float getRandomNr()
{
    float x = (rand()%1000) /1000.0;    //rand() liefert zufalls integer mit
    return x; 
}

/** Spuckt einen Tupel an random Zahlen
*/
void getRandomPair(float &x,  float &y) // Pass by reference
{
   x = getRandomNr();
   y = getRandomNr();
}

/** Einstiegspunkt
*/
int main()
{
//    trogdisplay::init(10);
    srand(time(0));                     // Seed  Pseudo Random Number Generator mit momentaner zeit 
                                        // mehr infos: https://en.wikipedia.org/wiki/Random_seed
    int imViertelKreis = 0;             // Zaehlvariable f�r Tropfen im Viertelkreis
    int gesamt;                         // Zaehlvariable f�r Gesamtanzahl von Tropfen.

    for(gesamt = 0; gesamt < 100000; ++gesamt) //Lass es 100000 Tropfen regnen
    {
      float x, y;                           // Variablen f�r Tropfenposition
      getRandomPair(x, y);                  // Ermittle tropfenposition
//      trogdisplay::update(x,y);             //display code
      bool bedingung = (x*x + y*y < 1);     // Falls tropfen im Viertelkreis
      if(bedingung)
      {
        imViertelKreis += 1;
      }
//      trogdisplay::display();              // display code (auskommentieren fuer schnelleren durchlauf)
    }

    float pi = (float)imViertelKreis/(float)gesamt; 
    pi *= 4;

    cout <<"Pi ist: " << pi << endl; 
    return 0; 
}


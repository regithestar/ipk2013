#include <iostream>
#include <stdlib.h>
#include <time.h>



/* Namespace with functions for displaying and updating the
   Regentrog
   Usage: 
    trogdisplay::init(10)   // create a display with size 10;
    float x = computeX(....)// compute new raindrop coordinate
        float y = ...;
    trogdisplay::update(x,y);
    ...
    trogdisplay::display();
*/
namespace trogdisplay
{
    /* Memory holding the Ascii Image*/
    char  trog[100][100];
    /* size of the ascii image */
    int size = 0;
    
    /* initialize empty ascii image, size is resolution*/
    void init(int resolution = 25);

    /* update trog image with new point x, y
    */
    void update(float x, float y);

    /* display trog
    */
    void display();

}
/////// AB HIER DEFINITIONEN


void trogdisplay::init(int resolution)// Default argumente werden nur einmal vergeben
{
    size = resolution;
    for(int ii =0; ii < size; ++ii)
    {
        for(int jj = 0; jj < size; ++jj)
        {
            float x = ii/(float)size;
            float y = jj/(float)size;
            if(x*x + y*y <=1)
        {
                trog[ii][jj] = ' ';
            }
            else
            {
                trog[ii][jj] = '.';
            }
        }
    }
}

/* update trog image with new point x, y
*/
void trogdisplay::update(float x, float y)
{
    trog[(int)(x*size)][(int)(y*size)] = '*';
}

/* display trog
*/
void trogdisplay::display()
{
    for(int ii =0; ii < size; ++ii)
    {
        for(int jj = 0; jj < size; ++jj)
        {
            std::cout << trog[ii][jj];
        }
        std::cout << std::endl;
    }
    std::cout << "Press Enter Key to continue\n";
    std::cin.ignore(0, '\n');
    std::cin.get();
}

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <hero.h>
#include <zombie.h>
#include <board_display.h>
const int board_size = 10;
using namespace std;


int main()
{
    srand(time(0));
    Hero hero(2,5);
    Zombie zombiehorde[10];
    Spielfeld zombieland;

    while(true)
    {
        zombieland.reset();
        zombieland.drawHero(hero);
        for(int ii =0; ii < 10; ++ii)
            zombieland.drawZombie(zombiehorde[ii]);

        zombieland.display();
        if(!(hero.isAlive))
        {
            cout << "BRAIINZZZZ... \n";
            break;
        }
        int nZombieAlive = 0;
        for(int ii = 0; ii < 10; ++ii)
         nZombieAlive
            += (zombiehorde[ii].isAlive)?
               1:0;
        if(nZombieAlive == 0)
        {
            cout << "Saved the World...Again \n";
            break;
        }

        //if(gameover criterion)
        //    break;
        char input;
        cout << "wasd to move...q to exit\n";
        input = getch();
        if(input == 'q')
        {
            cout << "User exited\n";
            break;
        }

        //Aktion des helden
        hero.move(input);
        for(int ii = 0; ii < 10; ++ii)
            hero.kill(zombiehorde[ii]);

        zombieland.reset();
        zombieland.drawHero(hero);
        for(int ii =0; ii < 10; ++ii)
            zombieland.drawZombie(zombiehorde[ii]);
        zombieland.display();
        cout << "Any key for zombie move\n";
        getch();
        // Aktion der Zombiehorde
        for(int ii = 0; ii <10; ++ii)
        {
            Zombie & zombie = zombiehorde[ii];
            zombie.move();
            zombie.eat(hero);
        }
    }
    return 0;
}

#include <board_display.h>
#include <hero.h>
#include <zombie.h>
#include <iostream>
void Spielfeld::reset()
{
    for(int ii = 0; ii < 10; ++ii)
    {
        for(int jj = 0; jj < 10; ++jj)
        {
            data[ii][jj] = ' ';
        }
    }
}
void Spielfeld::drawHero(Hero &hero)
{
    if(hero.isAlive)
        data[hero.x][hero.y] = 'H';
}

void Spielfeld::drawZombie(Zombie & zombie)
{
    if(zombie.isAlive)
        data[zombie.x][zombie.y] = 'Z';
}

void Spielfeld::display()
{
    for(int ii = 0; ii < 10; ++ii)
    {
        for(int jj = 0; jj < 10; ++jj)
        {
            std::cout << data[jj][ii];
        }
        std::cout << std::endl;
    }
}

Spielfeld::Spielfeld()
{
    reset();
}

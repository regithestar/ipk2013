#include <zombie.h>
#include <hero.h>
#include <stdlib.h>
#include <time.h>


Zombie::Zombie()
{
   x = rand()%10;
   y = rand()%3+7;
   isAlive = true;
}
void Zombie::move()
{
  int board_size = 10;
  int input = rand()%4;
   switch(input)
  {
    case 0:
      y +=1;
      break;
    case 1:
      y -=1;
      break;
    case 2:
      x +=1;
      break;
    case 3:
      x -=1;
      break;
  }
  x += board_size;
  y += board_size;
  x = x%board_size;
  y = y%board_size;
}


void Zombie::eat(Hero & hero)
{
    if(hero.x == this->x && hero.y == this->y)
        hero.isAlive = false;
}
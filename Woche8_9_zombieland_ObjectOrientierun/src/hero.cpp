#include <hero.h>
#include <zombie.h>
#include <iostream>

void Hero::move(char input)
{
  int board_size = 10;
  switch(input)
  {
    case 'w':
      y -=1;
      break;
    case 's':
      y +=1;
      break;
    case 'a':
      x -=1;
      break;
    case 'd':
      x +=1;
      break;
  }
  x += board_size;
  y += board_size;
  x = x%board_size;
  y = y%board_size;
}

Hero::Hero(int px, int py)
{
    x = px;
    y = py;
    isAlive = true;
}

void Hero::kill(Zombie & zombie)
{
    if(zombie.x == this->x && zombie.y == this->y)
    {
        std::cout << "ugh\n";
        zombie.isAlive = false;
    }
}
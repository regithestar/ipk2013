#ifndef HERO_H
#define HERO_H

struct Zombie; 


struct Hero
{
    int x;
    int y;
    bool isAlive;
    void move(char input);
    Hero(int px = 5, int py = 6);
    void kill(Zombie & zombie);
};

#endif
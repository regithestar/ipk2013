#ifndef ZOMBIE_H
#define ZOMBIE_H

struct Hero;    // Forward Declaration (Es gibt einen struct vom Typen Hero... wird weiter unten definiert)
struct Zombie
{
    int x;
    int y;
    bool isAlive;
    Zombie();
    void eat(Hero & hero);
    void move();
};
#endif
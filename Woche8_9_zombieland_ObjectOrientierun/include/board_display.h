#ifndef BOARD_DISPLAY_H__
#define BOARD_DISPLAY_H__

struct Hero;
struct Zombie;

struct Spielfeld
{
    char data[10][10];
    void reset();
    void drawHero(Hero &hero);
    void drawZombie(Zombie & zombie);
    void display();
    Spielfeld();
};

#endif //BOARD_DISPLAY_H__